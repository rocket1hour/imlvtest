from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='indexPage'),
    path('home/', views.home, name='homePage'),
    path('home2/', views.home2, name='homePage2'),
    path('detail/<str:space_id>', views.deltail, name='detail')
]