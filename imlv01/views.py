# from django.http import HttpResponse
# from django.template import loader
# shortcuts of two is
from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader
from .models import space_python

from django.http import Http404

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def home(request):
    try:
        lstSpacePython = space_python.objects.all()
        template = loader.get_template('views/index.html')
        context = {
            'lstSpacePython': lstSpacePython,
        }
    except space_python.DoesNotExist:
        raise Http404("Question does not exist")
    return HttpResponse(template.render(context, request))

def home2(request):
    lstSpacePython = space_python.objects.all()
    context = {'lstSpacePython': lstSpacePython}
    return render(request, 'views/index.html', context)

def deltail(request, space_id):
    lstSpacePython = space_python.objects.get(space_id=space_id)
    context = {'lstSpacePython': lstSpacePython}
    return render(request,"views/detail.html", context)
